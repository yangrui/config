

#### 下载安装

```
ETCD_VER=v3.4.0 \

GITHUB_URL=https://github.com/etcd-io/etcd/releases/download \
DOWNLOAD_URL=${GITHUB_URL} \

rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz \
rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test \

curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

tar -xvf  etcd-${ETCD_VER}-linux-amd64.tar.gz
 
cp etcd-${ETCD_VER}-linux-amd64/etcd /usr/local/bin
cp etcd-${ETCD_VER}-linux-amd64/etcdctl /usr/local/bin
```

#### 添加到环境变量

> export  ETCDCTL_API=3

#### 配置systemcd

```

$ cat /etc/systemd/system/etcd.service 
[Unit]
Description=Etcd Server
After=network.target
After=network-online.target
Wants=network-online.target
 
[Service]
Type=notify
WorkingDirectory=/data/etcd/  
ExecStart=/usr/local/bin/etcd  --config-file=/etc/etcd/etcd.conf
Restart=on-failure
LimitNOFILE=65536
 
[Install]
WantedBy=multi-user.target
```

####创建配置文件及目录

```
mkdir /data/etcd /etc/etcd

```

#### 三个节点配置 

```
# ip  192.168.6.142
$ cat /etc/etcd/etcd.conf
name: etcd-1
data-dir: /data/etcd
listen-client-urls: http://0.0.0.0:2379
advertise-client-urls: http://192.168.6.142:2379
listen-peer-urls: http://0.0.0.0:2380
initial-advertise-peer-urls: http://192.168.6.142:2380
initial-cluster: etcd-1=http://192.168.6.142:2380,etcd-2=http://192.168.6.143:2380,etcd-3=http://192.168.6.144:2380
initial-cluster-token: etcd-cluster-token
initial-cluster-state: new


# ip  192.168.6.143
$ cat /etc/etcd/etcd.conf 
name: etcd-2
data-dir: /data/etcd
listen-client-urls: http://0.0.0.0:2379
advertise-client-urls: http://192.168.6.143:2379
listen-peer-urls: http://0.0.0.0:2380
initial-advertise-peer-urls: http://192.168.6.143:2380
initial-cluster: etcd-1=http://192.168.6.142:2380,etcd-2=http://192.168.6.143:2380,etcd-3=http://192.168.6.144:2380
initial-cluster-token: etcd-cluster-token
initial-cluster-state: new


# ip  192.168.6.144
$ cat /etc/etcd/etcd.conf 
name: etcd-3
data-dir: /data/etcd
listen-client-urls: http://0.0.0.0:2379
advertise-client-urls: http://192.168.6.144:2379
listen-peer-urls: http://0.0.0.0:2380
initial-advertise-peer-urls: http://192.168.6.144:2380
initial-cluster: etcd-1=http://192.168.6.142:2380,etcd-2=http://192.168.6.143:2380,etcd-3=http://192.168.6.144:2380
initial-cluster-token: etcd-cluster-token
initial-cluster-state: new


```


####  查看集群状态

```
$ etcdctl endpoint status --cluster -w table
+---------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
|         ENDPOINT          |        ID        | VERSION | DB SIZE | IS LEADER | IS LEARNER | RAFT TERM | RAFT INDEX | RAFT APPLIED INDEX | ERRORS |
+---------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
| http://192.168.6.142:2379 | 514ed52039b2ae9d |   3.4.0 |   29 kB |      true |      false |       359 |         27 |                 27 |        |
| http://192.168.6.143:2379 | 71aadc4e9d2ed456 |   3.4.0 |   25 kB |     false |      false |       359 |         27 |                 27 |        |
| http://192.168.6.144:2379 | af1bd0647f52a7f0 |   3.4.0 |   25 kB |     false |      false |       359 |         27 |                 27 |        |
+---------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
```