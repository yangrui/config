#!/bin/bash

# wget -qO - https://openresty.org/package/pubkey.gpg | sudo apt-key add -
# apt-get -y install software-properties-common
# add-apt-repository -y "deb http://openresty.org/package/ubuntu $(lsb_release -sc) main"
# apt-get update
# apt-get install libpcre3-dev  libssl-dev perl make build-essential curl -y
# apt-get install openresty 

wget https://openresty.org/download/openresty-1.13.6.2.tar.gz
tar -xvf openresty-1.13.6.2.tar.gz
cd openresty-1.13.6.2
apt-get install libreadline-dev libncurses5-dev libpcre3-dev libssl-dev perl -y
cd openresty-1.13.6.2
# ./configure  --with-luajit --with-stream --with-http_iconv_module --with-http_realip_module --with-http_stub_status_module --with-http_ssl_module --with-http_sub_module --with-http_flv_module --with-http_gzip_static_module --with-pcre 
./configure  \
--with-luajit \
--with-stream \
--with-http_iconv_module \
--with-http_realip_module \
--with-http_stub_status_module \
--with-http_ssl_module \
--with-http_sub_module \
--with-http_flv_module \
--with-http_gzip_static_module \
--with-pcre ;

make;
make install;

echo 'export PATH=/usr/local/openresty/nginx/sbin:$PATH'>>/etc/profile;
echo 'export PATH=/usr/local/openresty/bin:$PATH'>>/etc/profile;
source /etc/profile

echo '# Stop dance for OpenResty
# A modification of the Nginx systemd script
# =======================
#
# ExecStop sends SIGSTOP (graceful stop) to the Nginx process.
# If, after 5s (--retry QUIT/5) OpenResty is still running, systemd takes control
# and sends SIGTERM (fast shutdown) to the main process.
# After another 5s (TimeoutStopSec=5), and if OpenResty is alive, systemd sends
# SIGKILL to all the remaining processes in the process group (KillMode=mixed).
#
# Nginx signals reference doc:
# http://nginx.org/en/docs/control.html
#
[Unit]
Description=A dynamic web platform based on Nginx and LuaJIT.
After=network.target
[Service]
Type=forking
PIDFile=/run/openresty.pid
ExecStartPre=/usr/local/openresty/bin/openresty -t -q -g 'daemon on; master_process on;'
ExecStart=/usr/local/openresty/bin/openresty -g 'daemon on; master_process on;'
ExecReload=/usr/local/openresty/bin/openresty -g 'daemon on; master_process on;' -s reload
ExecStop=-/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /run/openresty.pid
TimeoutStopSec=5
KillMode=mixed
[Install]
WantedBy=multi-user.target'>/etc/systemd/system/nginx.service

mkdir /var/log/nginx

ln -s /usr/local/openresty/nginx/conf/nginx.conf /etc/nginx.conf

systemctl daemon-reload 
systemctl start  nginx;
systemctl enable  nginx
