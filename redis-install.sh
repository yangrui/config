#!/bin/bash
#
#
echo "start download redis1.0.11 ....."
cd /usr/local/src
wget http://download.redis.io/releases/redis-4.0.11.tar.gz
tar -xvf redis-4.0.11.tar.gz
cd redis-4.0.11
echo "start make redis ..."
make
#
mkdir -p /usr/local/redis/bin /usr/local/redis/conf
cp /usr/local/src/redis-4.0.11/src/redis* /usr/local/redis/bin
cp /usr/local/src/redis-4.0.11/redis.conf /usr/local/redis/conf/6379.conf
cd /usr/local/redis/bin
rm -f *.h *.c *.o

echo 'export PATH=$PATH:/usr/local/redis/bin'>>/etc/profile
#
echo "set redis start onboot "
echo '#!/bin/sh
#
# Simple Redis init.d script conceived to work on Linux systems
# as it does use of the /proc filesystem.

### BEGIN INIT INFO
# Provides:     redis_6379
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Redis 6379
# Description:          Redis 6379
### END INIT INFO

REDISPORT=6379
EXEC=/usr/local/redis/bin/redis-server
CLIEXEC=/usr/local/redis/bin/redis-cli
PIDFILE=/usr/local/redis/redis_${REDISPORT}.pid
CONF="/usr/local/redis/conf/${REDISPORT}.conf"

case "$1" in
    start)
        if [ -f $PIDFILE ]
        then
                echo "$PIDFILE exists, process is already running or crashed"
        else
                echo "Starting Redis server..."
                $EXEC $CONF
        fi
        ;;
    stop)
        if [ ! -f $PIDFILE ]
        then
                echo "$PIDFILE does not exist, process is not running"
        else
                PID=$(cat $PIDFILE)
                echo "Stopping ..."
                kill ${PID}
                rm $PIDFILE
                #$CLIEXEC -p $REDISPORT shutdown
                while [ -x /proc/${PID} ]
                do
                    echo "Waiting for Redis to shutdown ..."
                    sleep 1
                done
                echo "Redis stopped"
        fi
        ;;
    *)
        echo "Please use start or stop as first argument"
        ;;
esac'>/etc/init.d/redisd

chmod +x /etc/init.d/redisd
cd /etc/init.d/
update-rc.d redisd defaults

#
apt install ruby -y
gem install redis
echo "ok ,please config redis ..."


