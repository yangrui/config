centos7  扩展根分区

+ 最近安装CentOS 7服务器，为了方便快速，选择了默认分区安装，但是centos7默认分区的root大小为50G，也就是说如果硬件分配的总大小超过50G，剩余的所有空间都会分配给home。

    这时候软件如果装在/usr/local目录下，并且data等数据文件也配置在root下，则必须在装机后调整root的大小，否则运行一段时间后很容易导致磁盘空间不足。基于这种情况，我们只需要将调整一下home分区的大小预留为20G，将其他的空间都分配给root即可。

    总体思路为：备份/home内容，然后将/home文件系统所在的逻辑卷删除，扩大/root文件系统，新建/home ，恢复/home内容

#### 一、卸载home 

 1.1 备份home分区文件 

> tar cvf /tmp/home.tar /home

 1.2 安装psmisc 

```  
yum install -y psmisc
//Psmisc软件包包含三个帮助管理/proc目录的程序，安装下列程序: fuser、 killall、pstree和pstree.x11(到pstree的链接)

//fuser 显示使用指定文件或者文件系统的进程的PID。

//killall 杀死某个名字的进程，它向运行指定命令的所有进程发出信号。

//pstree 树型显示当前运行的进程。

//pstree.x11 与pstree功能相同，只是在退出前需要确认。
```
1.3 卸载/home文件系统 

> umount /home
 如果提示无法卸载，是因为有进程占用/home，可以用下面的命令来停止占用的进程。  

> fuser -km /home/  

 1.4 删除/home所在的lv 

>  lvremove /dev/mapper/centos-home 

#### 二、扩大root

 2.1 扩展/root所在的lv

> 由于之前/home占用了342G的空间，故我考虑将加到/root里320G，剩下的留给/home。 

> lvextend -L +320G /dev/mapper/centos-root
>如果显示：logical volume centos/root successfully resized.则表示成功。

2.2 扩展/root文件系统 

> xfs_growfs /dev/mapper/centos-root
> 这时可以使用df -h 检查一下root文件系统的空间看是不是已经改变了。

#### 三、恢复home分区

 3.1 重新创建home LV

这里我们先创建一个1G的home LV，然后再将所有的空闲分区追加到home LV

> lvcreate -L 1G -n /dev/mapper/centos-home
> lvextend -l +100%FREE /dev/mapper/centos-home  

3.2 创建home文件系统

> mkfs.xfs  /dev/mapper/centos-home

3.3 挂载home文件系统
> mount /dev/mapper/centos-home  

3.4 恢复home文件系统  

```
tar xvf /tmp/home.tar -C /home/
cd /home/home/
mv * ../
```