#!/bin/bash
#

echo 'download and install node js'
wget http://cdn.npm.taobao.org/dist/node/v10.15.3/node-v10.15.3-linux-x64.tar.xz
tar -xvf node-v10.15.3-linux-x64.tar.xz -C /usr/local
ln -s /usr/local/node-v10.15.3-linux-x64/bin/node /usr/local/bin/node
ln -s /usr/local/node-v10.15.3-linux-x64/bin/npm /usr/local/bin/npm
ln -s /usr/local/node-v10.15.3-linux-x64/bin/npx /usr/local/bin/npx
npm config set registry https://registry.npm.taobao.org

echo 'set path'
echo '#set nodejs env  
export NODE_HOME=/usr/local/node-v10.15.3-linux-x64
export PATH=$NODE_HOME/bin:$PATH  
export NODE_PATH=$NODE_HOME/lib/node_modules:$PATH'>>/etc/profile


echo " npm install pm2 -g  start ....."
npm install pm2 -g
echo " .....ok"