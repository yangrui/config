### fluentd 配置文件

```
cat /etc/td-agent/td-agent.conf 

####
## Output descriptions:
##

# Treasure Data (http://www.treasure-data.com/) provides cloud based data
# analytics platform, which easily stores and processes data from td-agent.
# FREE plan is also provided.
# @see http://docs.fluentd.org/articles/http-to-td
#
# This section matches events whose tag is td.DATABASE.TABLE

<match docker.*>
  @type file
  compress gzip
  append             true
  path               /var/log/data/docker/${tag[1]}/${tag[1]}
  <format>
    @type            single_value
    message_key      log
  </format>
  <buffer tag,time>
    @type             file
    path              /var/log/data/buffer/
    timekey           3d
    timekey_wait      10m
    flush_mode        interval
    flush_interval    3s
  </buffer>

</match>
```


+ docker run -d --log-driver=fluentd  --log-opt fluentd-address=tcp://192.168.6.130:24224   --log-opt tag=docker.hello --log-opt fluentd-async-connect --name hello2 hello-world
