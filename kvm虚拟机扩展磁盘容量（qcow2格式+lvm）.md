1. 对虚拟机的qcow2镜像进行扩容
> qemu-img info <img-name>.qcow2 #查看磁盘信息
> qemu-img resize <img-name>.qcow2 +5G #例：增加5G
> 重启或用新镜像新建虚拟机
2. 用空闲的空间创建新分区
> 此时扩展的空间并没有加到现有的磁盘上，可以通过df -h查看
> 应该先用空闲空间新建分区，再扩充到目标分区
> 如果有图形界面就用图形界面吧，方便些，下面是通过命令创建分区
```
（1）因为新增加的空间还没有划分使用，所以要继续分区：

[root@localhost ~]# fdisk /dev/vda

WARNING: DOS-compatible mode is deprecated. It's strongly recommended to

         switch off the mode (command 'c') and change display units to

         sectors (command 'u').

Command (m for help): p
Disk /dev/vda: 16.1 GB, 16106127360 bytes
16 heads, 63 sectors/track, 31207 cylinders
Units = cylinders of 1008 * 512 = 516096 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000dca27
   Device Boot      Start         End      Blocks   Id  System
/dev/vda1   *           3        1018      512000   83  Linux
Partition 1 does not end on cylinder boundary.
/dev/vda2            1018       20806     9972736   8e  Linux LVM
Partition 2 does not end on cylinder boundary.

# 选n
Command (m for help): n

Command action
   e   extended
   p   primary partition (1-4)
# 选p
p

#下面3个基本用默认的
Partition number (1-4): 3 #分区号 前面1，2已经被用了
First cylinder (1-31207, default 1): 20807  # 起始位置
Last cylinder, +cylinders or +size{K,M,G} (20807-31207, default 31207): 31207 # 结束位置



Command (m for help): p


Disk /dev/vda: 16.1 GB, 16106127360 bytes

16 heads, 63 sectors/track, 31207 cylinders

Units = cylinders of 1008 * 512 = 516096 bytes

Sector size (logical/physical): 512 bytes / 512 bytes

I/O size (minimum/optimal): 512 bytes / 512 bytes

Disk identifier: 0x000dca27

   Device Boot      Start         End      Blocks   Id  System

/dev/vda1   *           3        1018      512000   83  Linux

Partition 1 does not end on cylinder boundary.

/dev/vda2            1018       20806     9972736   8e  Linux LVM

Partition 2 does not end on cylinder boundary.

/dev/vda3           20807       31207     5242104   83  Linux

Command (m for help): w

The partition table has been altered!
Calling ioctl() to re-read partition table.
WARNING: Re-reading the partition table failed with error 16: Device or resource busy.
The kernel still uses the old table. The new table will be used at
the next reboot or after you run partprobe(8) or kpartx(8)
Syncing disks.


 ls /dev/vda3 //如果没有这个分区，需要重启一下
```
3. 添加到 lvm并扩展（前提是有lvm）

```

[root@localhost ~]# pvcreate /dev/vda3  //创建物理卷
  Physical volume "/dev/vda3" successfully created

[root@localhost ~]# pvs //查看卷

  PV         VG       Fmt  Attr PSize PFree
  /dev/vda2  VolGroup     lvm2 a--  9.51g  0
  /dev/vda3            lvm2 ---  5.00g  5.00g

# 注意 VolGroup是上面已经存在lvm名，

[root@localhost ~]# vgextend VolGroup /dev/vda3  //VolGroup虚拟卷扩展，vda3加入到Vol
  Volume group "VolGroup" successfully extended


[root@localhost ~]# pvs
  PV         VG       Fmt  Attr PSize PFree
  /dev/vda2  VolGroup lvm2 a--  9.51g    0
  /dev/vda3  VolGroup lvm2 a--  5.00g    0

[root@localhost ~]# vgs    //VFree中5G
  VG       #PV #LV #SN Attr   VSize  VFree
  VolGroup   2   2   0 wz--n- 14.50g  5.00g   


[root@localhost ~]# lvs //查看逻辑卷
  LV    VG  Attr    LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv_root VolGroup -wi-ao----   8.54g
  lv_swap VolGroup -wi-ao---- 992.00m

[root@localhost ~]# lvextend -l +100%FREE /dev/VolGroup/lv_root  //扩展卷
  Size of logical volume VolGroup/lv_root changed from 8.54 GiB (2186 extents) to 13.54 GiB (3465 extents).
  Logical volume lv_root successfully resized


[root@localhost ~]# df -h
Filesystem            Size  Used Avail Use% Mounted on
/dev/mapper/VolGroup-lv_root 8.3G  685M  7.2G   9% /
tmpfs                246M     0  246M   0% /dev/shm
/dev/vda1              477M   33M  419M   8% /boot


# 注意此处resize2fs可能报错，因为centos7用的是xfs
# xfs文件系统 命令为：xfs_growfs /dev/VolGroup/lv_root  
# ext4文件系统命令为：resize2fs /dev/VolGroup/lv_root  

[root@localhost ~]# resize2fs /dev/VolGroup/lv_root  

resize2fs 1.41.12 (17-May-2010)
Filesystem at /dev/VolGroup/lv_root is mounted on /; on-line resizing required
old desc_blocks = 1, new_desc_blocks = 1
Performing an on-line resize of /dev/VolGroup/lv_root to 3548160 (4k) blocks.
The filesystem on /dev/VolGroup/lv_root is now 3548160 blocks long.


然后，df -h查看，应该已经扩展了

```