### redis cluster 配置

#### 安装 redis ,至少保证奇数个master 节点，推荐6个节点，3主3从  

+ 安装参考 ubuntu16.04 下 的安装脚本

```
% cat redis-install.sh 
#!/bin/bash
#
# 下载 安装
echo "start download redis1.0.11 ....."
cd /usr/local/src
wget http://download.redis.io/releases/redis-4.0.11.tar.gz
tar -xvf redis-4.0.11.tar.gz
cd redis-4.0.11
echo "start make redis ..."
make
#
mkdir -p /usr/local/redis/bin /usr/local/redis/conf
cp /usr/local/src/redis-4.0.11/src/redis* /usr/local/redis/bin
cp /usr/local/src/redis-4.0.11/redis.conf /usr/local/redis/conf/6379.conf
cd /usr/local/redis/bin
rm -f *.h *.c *.o

echo 'export PATH=$PATH:/usr/local/redis/bin'>>/etc/profile
# 设置开机启动
echo "set redis start onboot "
echo '#!/bin/sh
#
# Simple Redis init.d script conceived to work on Linux systems
# as it does use of the /proc filesystem.

### BEGIN INIT INFO
# Provides:     redis_6379
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Redis 6379
# Description:          Redis 6379
### END INIT INFO

REDISPORT=6379
EXEC=/usr/local/redis/bin/redis-server
CLIEXEC=/usr/local/redis/bin/redis-cli
PIDFILE=/usr/local/redis/redis_${REDISPORT}.pid
CONF="/usr/local/redis/conf/${REDISPORT}.conf"

case "$1" in
    start)
        if [ -f $PIDFILE ]
        then
                echo "$PIDFILE exists, process is already running or crashed"
        else
                echo "Starting Redis server..."
                $EXEC $CONF
        fi
        ;;
    stop)
        if [ ! -f $PIDFILE ]
        then
                echo "$PIDFILE does not exist, process is not running"
        else
                PID=$(cat $PIDFILE)
                echo "Stopping ..."
                kill ${PID}
                rm $PIDFILE
                #$CLIEXEC -p $REDISPORT shutdown
                while [ -x /proc/${PID} ]
                do
                    echo "Waiting for Redis to shutdown ..."
                    sleep 1
                done
                echo "Redis stopped"
        fi
        ;;
    *)
        echo "Please use start or stop as first argument"
        ;;
esac'>/etc/init.d/redisd

chmod +x /etc/init.d/redisd
cd /etc/init.d/
update-rc.d redisd defaults
echo  "install ruby and ruby-redis"
# 安装ruby 
apt install ruby -y
# 安装ruby reids以来，千万不要安装最新版，安装3.x 版，不然会在 redis 集群重新（reshard）分片故障
#  [ERR] Calling MIGRATE ERR Syntax error, try CLIENT (LIST | KILL | GETNAME | SETNAME | PAUSE | REPLY)
gem install redis -v 3.3.5 
echo "ok ,please config redis ..."


```

####  配置redis ( /usr/local/redis/conf/6379)

+ 主要内容如下  

```
#bind 127.0.0.1  （注释掉或者更改，为了能够远程访问）
protected-mode no     （关闭保护模式，不然无法远程访问）
port 6379       （注意端口）
daemonize yes  （后台运行）
pidfile "/usr/local/redis/redis_6379.pid"    
logfile "/usr/local/redis/redis-server.log"  （日志文件）
dir "/usr/local/redis"  (持久化文件位置)

# 以下为密码设置，请确保所有节点都一直
masterauth "123456"     (cluster 密码)
requirepass "123456"    （密码）

# 以下为集群配置
cluster-enabled yes  (开启cluster)
cluster-config-file "/usr/local/redis/conf/nodes-6379.conf"    (每个节点配置文件，只需要指明位置，由cluster自动生成，不要人为改变)
cluster-node-timeout 5000 （超时时间—毫秒—，超时则认为节点死亡，重新选举一个slave 成为master）
appendonly yes
```
+ 也可以在正常运行时设置密码  
```
# 每个节点设置密码
config set masterauth 123456
config set requirepass 123456
auth 123456
config rewrite
```
+ 为了使 redis-trib.rb 可以使用，修改  /var/lib/gems/2.3.0/gems/redis-4.1.0/lib/redis/client.rb 中的密码

> 这个文件可以通过以下方式找到
> % find / -name client.rb
> /usr/lib/ruby/2.3.0/xmlrpc/client.rb
> /var/lib/gems/2.3.0/gems/redis-4.1.0/lib/redis/client.rb

```
% cat /var/lib/gems/2.3.0/gems/redis-4.1.0/lib/redis/client.rb
require_relative "errors"
require "socket"
require "cgi"

class Redis
  class Client

    DEFAULTS = {
      :url => lambda { ENV["REDIS_URL"] },
      :scheme => "redis",
      :host => "127.0.0.1",
      :port => 6379,
      :path => nil,
      :timeout => 5.0,
      :password => "123456",
      :db => 0,

```

#### 用redis-trib.rb 创建集群 --replicas 1 选项表示 每个master有一个 slave，后面是每个redis实例，然后会推荐一个 分配方案，输入yes  接受    

```
% redis-trib.rb create --replicas 1 192.168.249.128:6379 192.168.249.128:6380 192.168.249.129:6379 192.168.249.129:6380 192.168.249.130:6379 192.168.249.130:6380
>>> Creating cluster
>>> Performing hash slots allocation on 6 nodes...
Using 3 masters:
192.168.249.128:6379
192.168.249.129:6379
192.168.249.130:6379
Adding replica 192.168.249.129:6380 to 192.168.249.128:6379
Adding replica 192.168.249.130:6380 to 192.168.249.129:6379
Adding replica 192.168.249.128:6380 to 192.168.249.130:6379
M: e987a750cb11476326709b6ae5119a588952209e 192.168.249.128:6379
   slots:0-5460 (5461 slots) master
S: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   replicates 40d4c10b8ac251ab062b63976305bca31030a9df
M: 0f1b460a3c4fc5ff83399bd1803fdf2990b82727 192.168.249.129:6379
   slots:5461-10922 (5462 slots) master
S: 764f7514a47bdc7afacca9964977b1a66cadc04e 192.168.249.129:6380
   replicates e987a750cb11476326709b6ae5119a588952209e
M: 40d4c10b8ac251ab062b63976305bca31030a9df 192.168.249.130:6379
   slots:10923-16383 (5461 slots) master
S: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   replicates 0f1b460a3c4fc5ff83399bd1803fdf2990b82727
Can I set the above configuration? (type 'yes' to accept): yes
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join..
>>> Performing Cluster Check (using node 192.168.249.128:6379)
M: e987a750cb11476326709b6ae5119a588952209e 192.168.249.128:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   slots: (0 slots) slave
   replicates 0f1b460a3c4fc5ff83399bd1803fdf2990b82727
S: 764f7514a47bdc7afacca9964977b1a66cadc04e 192.168.249.129:6380
   slots: (0 slots) slave
   replicates e987a750cb11476326709b6ae5119a588952209e
S: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots: (0 slots) slave
   replicates 40d4c10b8ac251ab062b63976305bca31030a9df
M: 0f1b460a3c4fc5ff83399bd1803fdf2990b82727 192.168.249.129:6379
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
M: 40d4c10b8ac251ab062b63976305bca31030a9df 192.168.249.130:6379
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.

```

+ 至此，cluster已经启动

##### 【一次测试】
> 尝试停止一个master  192.168.249.128:6379  然后后查看集群

```
% redis-trib.rb check localhost:6379
>>> Performing Cluster Check (using node localhost:6379)
M: e987a750cb11476326709b6ae5119a588952209e localhost:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   slots: (0 slots) slave
   replicates 0f1b460a3c4fc5ff83399bd1803fdf2990b82727
S: 764f7514a47bdc7afacca9964977b1a66cadc04e 192.168.249.129:6380
   slots: (0 slots) slave
   replicates e987a750cb11476326709b6ae5119a588952209e
M: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots:10923-16383 (5461 slots) master
   0 additional replica(s)
M: 0f1b460a3c4fc5ff83399bd1803fdf2990b82727 192.168.249.129:6379
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.

```
> 可以看到 192.168.249.128:6380 自动成为master 节点
> 再次开启192.168.249.128:6379  可以看到 192.168.249.128:6379 是 192.168.249.128:6380 的从节点

```
% redis-trib.rb check localhost:6379
>>> Performing Cluster Check (using node localhost:6379)
M: e987a750cb11476326709b6ae5119a588952209e localhost:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   slots: (0 slots) slave
   replicates 0f1b460a3c4fc5ff83399bd1803fdf2990b82727
S: 764f7514a47bdc7afacca9964977b1a66cadc04e 192.168.249.129:6380
   slots: (0 slots) slave
   replicates e987a750cb11476326709b6ae5119a588952209e
M: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
M: 0f1b460a3c4fc5ff83399bd1803fdf2990b82727 192.168.249.129:6379
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
S: 40d4c10b8ac251ab062b63976305bca31030a9df 192.168.249.130:6379
   slots: (0 slots) slave
   replicates cc49b74f6629dbfed4b1e28473a6216a7f46b0f1
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.

```
> 直接停掉 一个master 和它的slave,依然正常运行

```
% redis-trib.rb check localhost:6379
>>> Performing Cluster Check (using node localhost:6379)
M: e987a750cb11476326709b6ae5119a588952209e localhost:6379
   slots:0-5460 (5461 slots) master
   0 additional replica(s)
M: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   slots:5461-10922 (5462 slots) master
   0 additional replica(s)
M: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: 40d4c10b8ac251ab062b63976305bca31030a9df 192.168.249.130:6379
   slots: (0 slots) slave
   replicates cc49b74f6629dbfed4b1e28473a6216a7f46b0f1
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
```

#### 添加节点
> 查考[https://www.cnblogs.com/xlxue/p/7777257.html](https://www.cnblogs.com/xlxue/p/7777257.html)
> 有新的实例后,运行以下命令添加master 
```
 redis-trib.rb add-node 192.168.10.219:6378 192.168.10.219:6379  
```

> 192.168.10.219:6378是新增的节点 192.168.10.219:6379集群任一个旧节点
> 添加从节点  
```
redis-trib.rb add-node --slave --master-id 03ccad2ba5dd1e062464bc7590400441fafb63f2 192.168.10.220:6385 192.168.10.219:6379  
```
+ 新添加的master 没有分配slot ,是不会有数据存进去的
+ 重新分配slot

```
% redis-trib.rb reshard 192.168.249.131:6379
>>> Performing Cluster Check (using node 192.168.249.131:6379)
M: 7aa992d4da0aa4eb46666f7030bead1310be85cf 192.168.249.131:6379
   slots:10923-14255 (3333 slots) master
   1 additional replica(s)
S: 764f7514a47bdc7afacca9964977b1a66cadc04e 192.168.249.129:6380
   slots: (0 slots) slave
   replicates e987a750cb11476326709b6ae5119a588952209e
M: e987a750cb11476326709b6ae5119a588952209e 192.168.249.128:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
M: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots:14256-16383 (2128 slots) master
   1 additional replica(s)
M: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
S: 40d4c10b8ac251ab062b63976305bca31030a9df 192.168.249.130:6379
   slots: (0 slots) slave
   replicates cc49b74f6629dbfed4b1e28473a6216a7f46b0f1
S: 0f1b460a3c4fc5ff83399bd1803fdf2990b82727 192.168.249.129:6379
   slots: (0 slots) slave
   replicates d69f049b3e33d507eb9973dd098053df5e084ee4
S: ca40892a2c115a035fc7850d3959583a953b41df 192.168.249.131:6380
   slots: (0 slots) slave
   replicates 7aa992d4da0aa4eb46666f7030bead1310be85cf
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
How many slots do you want to move (from 1 to 16384)? 2  （这里输入要分配的slot数量，小于16384）
What is the receiving node ID? 7aa992d4da0aa4eb46666f7030bead1310be85cf （输入分配至那个节点，输入id 回车）
Please enter all the source node IDs.
  Type 'all' to use all the nodes as source nodes for the hash slots.
  Type 'done' once you entered all the source nodes IDs.
Source node #1:cc49b74f6629dbfed4b1e28473a6216a7f46b0f1  （从那个节点取出slot给它分配,all表示全部节点，输入节点id后回车）
Source node #2:done （输入done 表示下一步，然后会推荐一个方案，输入yes同意， 完成分配）

Ready to move 2 slots.
  Source nodes:
    M: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots:14256-16383 (2128 slots) master
   1 additional replica(s)
  Destination node:
    M: 7aa992d4da0aa4eb46666f7030bead1310be85cf 192.168.249.131:6379
   slots:10923-14255 (3333 slots) master
   1 additional replica(s)
  Resharding plan:
    Moving slot 14256 from cc49b74f6629dbfed4b1e28473a6216a7f46b0f1
    Moving slot 14257 from cc49b74f6629dbfed4b1e28473a6216a7f46b0f1
Do you want to proceed with the proposed reshard plan (yes/no)? yes
Moving slot 14256 from 192.168.249.128:6380 to 192.168.249.131:6379: 
Moving slot 14257 from 192.168.249.128:6380 to 192.168.249.131:6379:
```

> 可以再check一下
```
% redis-trib.rb check 192.168.249.131:6379  
>>> Performing Cluster Check (using node 192.168.249.131:6379)
M: 7aa992d4da0aa4eb46666f7030bead1310be85cf 192.168.249.131:6379
   slots:10923-14257 (3335 slots) master
   1 additional replica(s)
S: 764f7514a47bdc7afacca9964977b1a66cadc04e 192.168.249.129:6380
   slots: (0 slots) slave
   replicates e987a750cb11476326709b6ae5119a588952209e
M: e987a750cb11476326709b6ae5119a588952209e 192.168.249.128:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
M: cc49b74f6629dbfed4b1e28473a6216a7f46b0f1 192.168.249.128:6380
   slots:14258-16383 (2126 slots) master
   1 additional replica(s)
M: d69f049b3e33d507eb9973dd098053df5e084ee4 192.168.249.130:6380
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
S: 40d4c10b8ac251ab062b63976305bca31030a9df 192.168.249.130:6379
   slots: (0 slots) slave
   replicates cc49b74f6629dbfed4b1e28473a6216a7f46b0f1
S: 0f1b460a3c4fc5ff83399bd1803fdf2990b82727 192.168.249.129:6379
   slots: (0 slots) slave
   replicates d69f049b3e33d507eb9973dd098053df5e084ee4
S: ca40892a2c115a035fc7850d3959583a953b41df 192.168.249.131:6380
   slots: (0 slots) slave
   replicates 7aa992d4da0aa4eb46666f7030bead1310be85cf
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.

```