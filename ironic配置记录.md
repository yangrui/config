#### ironic.conf 配置
    enabled_hardware_types = ipmi,redfish
    enabled_boot_interfaces = pxe
    enabled_console_interfaces = ipmitool-socat,no-console
    enabled_deploy_interfaces = iscsi,direct
    enabled_inspect_interfaces = inspector
    enabled_power_interfaces = ipmitool,redfish
    enabled_raid_interfaces = agent


#### glance添加
```
glance image-create --name my-kernel --visibility public --disk-format aki --container-format bare < my-image.vmlinuz
glance image-create --name my-image.initrd --visibility public --disk-format ari --container-format bare < my-image.initrd
glance image-create --name my-image --visibility public --disk-format qcow2 --container-format bare --property kernel_id=<uuid-my-image.vmlinuz> --property ramdisk_id=<uuid-my-image.initrd> < user-image.qcow2  
```
```
openstack image create --disk-format aki --container-format aki --public \
  --file /etc/kolla/config/ironic/ironic-agent.kernel deploy-vmlinuz

openstack image create --disk-format ari --container-format ari --public \
  --file /etc/kolla/config/ironic/ironic-agent.initramfs deploy-initrd
```
#### glance 查看
```

  | ID                                   | Name                      |
  +--------------------------------------+---------------------------+
  | 022c1a82-5bab-4cb6-929f-42825cc95051 | centos7-baremetal.initrd  |
  | c05b5cda-8bc3-429e-8951-a785f3b5fe65 | centos7-baremetal.qcow2   |
  | a48eb4db-8f4c-4ec5-bbe4-d9b7c8dfa62c | centos7-baremetal.vmlinuz |
  | f09bb38a-2ae8-4896-94d4-0df31a51ebf9 | cirros                    |
  | 2d1ee833-c384-41af-8f05-e49658955fc3 | deploy-initrd             |
  | 78157554-5f8c-4c0d-897e-186e948f80cf | deploy-vmlinuz            |
  | e8cf94e8-949a-45d5-8a67-9b64a9969a95 | ironic-agent-initramfs    |
  | aefe0200-e1bb-4299-b1da-06acd035ed30 | ironic-agent-kernel       |
  | 79b3483d-9ede-4c9d-8b6f-4192c8d7d1e3 | ironic-image              |
  +--------------------------------------+---------------------------+  
```


#### node创建
+ 创建节点
> openstack baremetal node create \
  --driver ipmi --name <custom_node_name> \
  --driver-info ipmi_port=<ipmi_port> \
  --driver-info ipmi_username=<admin_user> \
  --driver-info ipmi_password=<password> \
  --driver-info ipmi_address=<ip_address> \
  --resource-class baremetal-resource-class \
  --property cpus=1 \
  --property cpu_arch=x86_64 \
  --property memory_mb=512 \
  --property local_gb=5 \
  --driver-info deploy_kernel=<uuid_deploy-vmlinuz> \
  --driver-info deploy_ramdisk=<uuid_deploy-initrd>
+ 创建 
 > openstack baremetal node create \
  --driver ipmi --name bare1 \
  --driver-info ipmi_port=623 \
  --driver-info ipmi_username=admin \
  --driver-info ipmi_password=admin \
  --driver-info ipmi_address=193.168.177.238 \
  --resource-class first-class \
  --property cpus=1 \
  --property memory_mb=2048 \
  --property local_gb=10 \
  --driver-info deploy_kernel=78157554-5f8c-4c0d-897e-186e948f80cf \
  --driver-info deploy_ramdisk=2d1ee833-c384-41af-8f05-e49658955fc3
+ 添加节点信息
>  ironic node-update 101d4c8b-6920-45de-ba56-d2c1305cade7 \
            add instance_info/image_source=f09bb38a-2ae8-4896-94d4-0df31a51ebf9 \
            instance_info/kernel=78157554-5f8c-4c0d-897e-186e948f80cf \
            instance_info/ramdisk=2d1ee833-c384-41af-8f05-e49658955fc3 \
            instance_info/root_gb=3   


+ 查看节点
```
   [root@ctrl-01 kvmdir]# ironic node-list
    The "ironic" CLI is deprecated and will be removed in the S* release. Please use the "openstack baremetal" CLI instead.
    +--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
    | UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
    +--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
    | 689504e0-d885-4819-a530-e1f748859f71 | test           | None          | None        | enroll             | False       |
    | 022b6968-c00d-4bdc-a9de-9a31d8c66c77 | hexinqiu01     | None          | None        | enroll             | False       |
    | 99293db7-bbfa-4fc3-8fc5-5623e5c449cf | centos7-test-5 | None          | power on    | available          | False       |
    | ecfc7585-6f5d-45ae-8ba7-b760cf725c06 | centos4        | None          | power off   | available          | False       |
    | 44e2b3ae-b365-4888-9176-05ab29b8166b | centos5        | None          | power off   | available          | False       |
    +--------------------------------------+----------------+---------------+-------------+--------------------+-------------+  
```
#### node信息添加
```
  ironic node-update <node-uuid> \
            add instance_info/image_source=<uuid-image> \
            instance_info/kernel=<uuid_deploy-vmlinuz> \
            instance_info/ramdisk=<uuid_deploy-initrd> \
            instance_info/root_gb=3   
```
#### flavor
> openstack flavor create --ram 512 --disk 1 --vcpus 1 my-baremetal-flavor

+ 注意上面的node --resource-class 属性  baremetal-resource-class
```  
 特征可以是标准的或定制的。标准特征列在 os_traits库中。自定义特征必须满足以下要求：                   https://docs.openstack.org/ironic/latest/install/configure-nova-flavors.html

  1  以前缀为 CUSTOM_
  2  仅包含大写字母A到Z，数字0到9或下划线
  3  长度不超过255个字符
裸机节点最多可以有50个特征。
```
#### 设置flavor
+ openstack flavor set my-baremetal-flavor --property resources:CUSTOM_BAREMETAL_RESOURCE_CLASS=1
  

#### 绑定端口
+ openstack baremetal port create <mac-Addrress> --node <node-UUID>

+ openstack baremetal port create 00:19:99:3E:C5:A8 --node e6a7f380-a15e-4f8c-b4df-39225c4881c8
``` 
    [root@ctrl-01 kvmdir]# ironic port-list

    The "ironic" CLI is deprecated and will be removed in the S* release. Please use the "openstack baremetal" CLI instead.
    +--------------------------------------+-------------------+
    | UUID                                 | Address           |
    +--------------------------------------+-------------------+
    | c0cafe94-0326-4f7d-9c36-31d285392825 | 00:19:99:3e:c5:a8 |
    | 429a12bf-c09f-49c7-8a53-7f41c00f77ba | 52:54:00:00:c6:0d |
    | dea8974f-7860-41bb-ba45-c54b57c564f6 | fe:54:00:1c:8a:b4 |
    +--------------------------------------+-------------------+
```

+ openstack baremetal node manage <node-uuid>     #状态变为 provision_state    | manage
+ openstack baremetal node provide <node-uuid>    #状态变为 provision_state    | available 
#### 创建server
+ openstack server create --image <image-name or uuid> --flavor my-baremetal-flavor --key-name mykey --network public1 <custom-server-name>

>   openstack server create --image cirros --flavor bare1 --network public1 bare-machine











#### 验证 裸机 电源状态 
    ipmitool -I lanplus -H <ip-address> -U <username> -P <password> chassis power status

    如果上述命令没有返回裸机服务器的电源状态，请检查

    ipmitool已安装并可通过$PATH环境变量获得。
    裸机服务器上的IPMI控制器已打开。
    在命令中传递的IPMI控制器凭据和IP地址是正确的。
    导体节点具有到IPMI控制器的路由。只需从导体节点ping IPMI控制器IP即可检查。

    